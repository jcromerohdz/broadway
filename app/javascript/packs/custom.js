const toggle = document.querySelector("#toggle")
const category = document.querySelector("#category")
const menu = document.querySelector("#dropdown")

const dropmenu = () => {
    if (menu.classList.contains('hidden')) {
        menu.classList.remove('hidden')
    } else {
        menu.classList.add('hidden')
    }
}

toggle.addEventListener('click', () => {
    dropmenu()
})

category.addEventListener('click', () => {
    dropmenu()
})
